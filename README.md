# Privacy Preserving Technologies

## This repo also contains WIKI pages on Literature Review 

[[_TOC_]]

## Libraries and frameworks for secure and private ML

#### 1. NVIDIA Clara Train Examples

Clara Train SDK is a domain optimized developer application framework that includes APIs for AI-Assisted Annotation, making any medical viewer AI capable and v4.0 enables a MONAI based training framework with pre-trained models to start AI development with techniques such as Transfer Learning, Federated Learning, and AutoML.

Link to [Clara repo](https://github.com/NVIDIA/clara-train-examples)

This repo contains Jupyter Notebooks to help you explore the features and capabilities of Clara Train, including AI-Assisted Annotation, AutoML, and Federated Learning.

#### 2. PySyft

PySyft is a Python library for secure and private Deep Learning

PySyft decouples private data from model training, using Federated Learning, Differential Privacy, and Encrypted Computation (like Multi-Party Computation (MPC) and Homomorphic Encryption (HE)) within the main Deep Learning frameworks like PyTorch and TensorFlow. 

Link to [PySyft examples](https://github.com/OpenMined/PySyft/tree/main/packages/syft/examples)

#### 3. TensorFlow Federated Tutorials
Set of tutorials on How to Buid Your Own Federated Learning Algorithm

Goals:
Understand the general structure of federated learning algorithms.

Explore the Federated Core of TFF.

Use the Federated Core to implement Federated Averaging directly

[Link to the tutorials](https://safe.menlosecurity.com/https://github.com/tensorflow/federated/tree/master/docs/tutorials
).
## Open sourced healthcare datases
Data Sources:

[SageBionetworks: dHealth Digital Health Knowledge Portal](https://dhealth.synapse.org/Explore/Data)

[OWEAR](https://www.owear.org/open-data-sets) 

[PhysioNet](https://www.physionet.org/about/database/)

[IEEEDataPort](https://safe.menlosecurity.com/https://ieee-dataport.org/datasets)

### Wearable Sensors 

#### a. ResearchKit by Apple
[The Asthma Health App (AHA)](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5559298/) and [MyHeart Counts](https://www.nature.com/articles/s41597-019-0016-7) were one of the first of five ResearchKit apps   wherein participants contributed data via an iPhone application built using Apple’s ResearchKit framework and consented to make this data available freely for further research applications

MyHeart Counts Study is Stanford Mobile Cardiovascular Health Study. 

Data: demographic; app captures: body measurements,  physical activity data; motion data from iPhone sensor signals; self-reported cardiovascular disease.

Examples of client interactions with these data are provided in [GitHub](https://github.com/AshleyLab/myheartcounts/tree/DataReleaseManuscript)

The Asthma Health App a research study on whether using an asthma-specific mobile health application downloaded onto a user’s iPhone helped the user monitor asthma symptoms. The study was completed in 2016 and the app is no longer available for use.

Data: active data collection from surveys; passive data collection using the devices GPS and linked it along with air-quality reports and weather reports.

Data access: restricted

#### b. In-hospital physical activity measured with a new Bosch accelerometer sensor system
Year:2016

Objective: To predict duration of hospitalization and 30-day readmission

Sample size: 57

Data: 
1. Demographic  data
2. Clinical: laboratory data(C-reactive protein (CRP), creatinine, white blood cell count, and hemoglobin concentration),
 heart rate, body temperature, blood pressure, blood oxygen saturation, and number of prescribed medications
3. Sensor: Acceleration (acc), Angular rate (gyro), Light, Temperature, Humidity, and Air pressure, does not contain any form of derived "physical activities"  (raw format, need to be processed with an activity recognition algorithm)

[Link to dataset](https://www.physionet.org/content/hospital-activity-bosch/1.0/)

Data access: restricted

#### c. Motion and heart rate from a wrist-worn wearable and labeled sleep from polysomnography
Year: 2017-2019

Objective: To classify sleep based on acceleration and photoplethymography-derived heart rate from the Apple Watch

Sample Size: 30

Data: 

 1. motion (acceleration): Recorded from the Apple Watch 
 2. heart rate (bpm): Recorded from the Apple Watch 
 3. steps (count): Recorded from the Apple Watch
 4. labeled sleep: Recorded from polysomnography

[Link to dataset](https://www.physionet.org/content/sleep-accel/1.0.0/)

Data access: open

#### d. Long term movement monitoring dataset

Year 2013

Objective: to study gait, stability, and fall risk

Sample size: 71

Data:

1. Demographic data
2. Clinical: the results of clinical tests
3. High resolution accelerometer recording files. The 3 day 3D recordings.

[Link to dataset](https://www.physionet.org/content/ltmm/1.0.0/)

Data access: open
